#!/bin/sh

WD=$(pwd)

if [ -d 'reveal.js' ]; then
    cd reveal.js || exit
    git pull
    cd "$WD" || exit
else
    git clone https://github.com/hakimel/reveal.js.git 'reveal.js'
fi

pandoc \
    --slide-level 2 \
    -t revealjs \
    -s talk.md \
    -o talk.html
