# Unix Philosophy

*by Clayton Voges*

## Introduction

- History of Unix (and C)
- History and Evolution of Unix Philosophy
- Relevance today
- Shortcomings
- Case studies

# History of Unix

## Development

![](https://upload.wikimedia.org/wikipedia/commons/1/1b/Ken_Thompson_and_Dennis_Ritchie--1973.jpg)

@ Bell Labs (AT&T):

- Ken Thompson
- Dennis Ritchie
- Brian Kernighan
- Douglas McIlroy
- Joe Ossanna

## Timeline

- 1969 - B programming language (for PDP-7 / PDP-11)
- 1969 - Unix development started (PDP-7)
- 1971 - Unix published internally (PDP-11)
- 1972 to 1973 - First C Compiler
- 1973 - Unix Version 5 (educational institutions only)
- 1975 - Unix Version 6 (first commercial release)
- 1981 - System III Unix
- 1983 - System V Unix
- 1989 - ANSI C Standard

## PDP-11

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Pdp-11-40.jpg/800px-Pdp-11-40.jpg" height="600px">

# Unix Philosophy

## The UNIX Time-Sharing System §8

*by Ken Thompson and Dennis Ritchie* (1974)

1. Make it easy to write, test, and run programs.
2. Interactive use instead of batch processing.
3. Economy and elegance of design due to size constraints ("salvation through suffering").
4. Self-supporting system: all Unix software is maintained under Unix.

## Bell System Technical Journal (pg 1902-1903)

*by Doug McIlroy* (1978)

1. Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".

## Bell System Technical Journal (pg 1902-1903)

*by Doug McIlroy* (1978)

2. Expect the output of every program to become the input to another, as yet unknown, program. Don't clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don't insist on interactive input.

## Bell System Technical Journal (pg 1902-1903)

*by Doug McIlroy* (1978)

3. Design and build software, even operating systems, to be tried early, ideally within weeks. Don't hesitate to throw away the clumsy parts and rebuild them.

## Bell System Technical Journal (pg 1902-1903)

*by Doug McIlroy* (1978)

4. Use tools in preference to unskilled help to lighten a programming task, even if you have to detour to build the tools and expect to throw some of them out after you've finished using them.

## Illustrations of these maxims are legion

<https://archive.org/details/bstj57-6-1899/page/n3/mode/2up?view=theater>

## A Quarter-Century of Unix

*by Peter H. Salus* (1994)

1. Write programs that do one thing and do it well.
2. Write programs to work together.
3. Write programs to handle text streams, because that is a universal interface.

# Shortcomings

## Small Software

- **The UNIX Time-Sharing System §8 #3** Economy and elegance of design due to size constraints (“salvation through suffering”).

## Small Software Criticism

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Jonathan_Blow_Gamelab_2018_crop_1.jpg/610px-Jonathan_Blow_Gamelab_2018_crop_1.jpg" height="200px">

> UNIX philosophy suffers from similar problems to microservices: ...big architectures end up very wrong and inefficient.

-- Jonathan Blow

## Large Software Case Studies

- Linux Kernel
- Systemd
- Web Browsers
- Most modern applications
- Most modern libraries
- etc

## Start over

- **Bell System Technical Journal (pg 1902-1903) #1** Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".

## Start over criticism

Case study:

- Git, Mercurial, Darcs, other version control software
- GitHub has 128M public repositories (as of 2020)
- Unix itself

## Interactive Use or Work Together

- **Bell System Technical Journal (pg 1902-1903) #2** Expect the output of every program to become the input to another, as yet unknown, program. Don’t clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don’t insist on interactive input.
- **A Quarter-Century of Unix #2** Write programs to work together.

## Interactive Use or Work Together Criticism

Case studies:

- Graphical User Interfaces are nice
- But having programs work together is still nice

## Text is the Universal Language

- **UNIX philosophy suffers from similar problems to microservices #3** Write programs to handle text streams, because that is a universal interface.

## Text Criticism

Case studies:

- Machine code
- Computer memory
- Prototype buffers aka protobuffs (ie - gRPC)
- Other efficient binary formats

# Conclusion

## Wrap up

## Questions?

Slides available @

https://gitlab.com/cvoges12/unix-philosophy-talk